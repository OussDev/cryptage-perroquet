//
//  comparaison cle perroquet.c
//  crypto entete fonction
//
//  Created by Oussama Boukh on 02/12/2022.
//  Copyright © 2022 Oussama Boukh. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "entete.h"

int rcPerr(char* cle)
{
    FILE* fpdef = NULL;
    char perroq[LG+1]="";
    

    fpdef = fopen("perroq.def", "r+");
    if (fpdef == NULL)
    {
        printf("\nErreur Open !!");
        exit(EXIT_FAILURE);
    }

    
   if(fpdef!=NULL)
    {
        fflush(stdin);
        fgets(perroq, LG+1, fpdef);
    }
    
    //printf("\nmon perroquet: %s",perroq);
    
    if (feof(fpdef))
    {
        printf("\nFin Fichier !!!");
    }
    
   if(strcmp(perroq,cle)==0)
      {
          printf("\nla clé est valide.\nLe déchiffrement est en cours...\n");
          return 1;
      }
      
      else
      {
          //printf("\nClé non valide:%s",cle);
          return 0;
      }
   
    
    int ret = fclose(fpdef);
    if (ret != 0)
    {
        printf("\nErreur Close !!");
        exit(EXIT_FAILURE);
    }

}
